<?php

namespace App\Providers;

use App\Services\ContestService;
use App\Services\UserSettings;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(UserSettings::class, function (){
            return new UserSettings();
        });

        $this->app->singleton(ContestService::class, function ( $app ){
            return new ContestService( $app->make( UserSettings::class ) );
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
