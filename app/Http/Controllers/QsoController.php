<?php

namespace App\Http\Controllers;



use App\Services\UserSettings;
use Illuminate\Support\Facades\App;
use App\Models\Qso;


class QsoController extends Controller
{

    function getController()
    {
        $userSettings = App::make(UserSettings::class);
        return ( $userSettings->getMode() === 'contest') ?
            App::make(QsoContestController::class ) :
            App::make( QsoLogController::class );
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function index()
    {
        return App::call([ $this->getController(), 'index' ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {

        return App::call([ $this->getController(), 'store' ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     */

    public function update()
    {

        return App::call([ $this->getController(), 'update' ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     */

    public function destroy($id)
    {
        if ( Qso::find($id) ) {
            Qso::find($id)->delete();
            return redirect()->back()->with('flash.message', 'Deleted Successfully.');
        }

        return redirect()->back()->with('flash.error', 'Delete failed.');

    }

}
