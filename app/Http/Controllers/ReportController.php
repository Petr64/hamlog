<?php

namespace App\Http\Controllers;

use App\Services\ContestService;
use App\Services\Report\Cabrillo;
use App\Services\Report\Csv;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class ReportController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function index(ContestService $contestService, Request $request)
    {

        $format_id = $request->get('format_id') ?? 'cabrillo';
        $output_id = $request->get('output_id') ?? 'scr';
        $band_id = $request->get('band_id') ?? '2';

        $temp = match ($format_id) {
            'csv' => Csv::getContent($contestService, $band_id),
            'cabrillo' => Cabrillo::getContent($contestService),
        };

        $settings = [
          'format' => [
              [ 'id' => 'cabrillo', 'name' => 'Cabrillo' ],
              [ 'id' => 'csv', 'name' => 'CSV' ],
          ],
          'output' => [
              [ 'id' => 'scr', 'name' => 'Screen online' ],
              [ 'id' => 'file', 'name' =>  'Download as file' ],
          ],
          'bands' => [
              [ 'id' => '2', 'name' => '144' ],
              [ 'id' => '07', 'name' => '433' ],
          ]

        ];

        return Inertia::render('Reports/Index', [
            'data' => $temp,
            'settings' => $settings,
            'format_id' => $format_id,
            'output_id' => $output_id,
            'band_id' => $band_id,
        ]);

    }


}
