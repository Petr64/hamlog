<?php

namespace App\Http\Controllers;


use App\Models\Band;
use App\Models\Mode;
use App\Services\ContestService;
use App\Services\UserSettings;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Qso;
use App\Models\QsoContest;
use Illuminate\Support\Facades\Validator;


class QsoContestController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     */
    public function index( UserSettings $userSettings, ContestService $contestService )
    {

        $log = Qso::with('contest')
            ->whereHas('contest',function(Builder $q) use( $contestService ) {
                $q->where('contest_id', $contestService->getContestId() );
            })
            ->orderByDesc('time')
            ->get();

        $bands = Band::all()->sortBy('order')->values();
        $modes = Mode::all()->sortBy('order')->values();

        $qso = $log->first();

        $new_rec = [
            'band_id' => $qso ? $qso->band_id : 40,
            'mode_id' => $qso ? $qso->mode_id : 'SSB',
            'tour' => $qso ? $qso->contest->tour : 1,
            'my_exchange' => $contestService->numCalc( $qso ),
            'type' => env('CONTEST_TYPE'),
            'time' => now(),
        ];

        $settings = [
            'contest_settings' => [
                'start'=> $contestService->getStartTime()->toISOString(),
                'duration'=> $contestService->getTourDurationInISO(),
                'tours'=> $contestService->getTourCount(),
                'locator' => $contestService->hasQthLocator(),
                'need_num_refresh' => $contestService->needNumRefresh(),
            ],
            'rig_apis' => $userSettings->getRigApis(),
            'rig_used' => $userSettings->getRigUsed(),
            'qth' => $userSettings->getQTH(),
        ];

        return Inertia::render('QsoContest', [
            'log' => $log, 'bands' => $bands, 'modes' => $modes, 'new_rec' => $new_rec, 'settings' => $settings,
        ]);
    }

    public function get_num($band_id, ContestService $contestService)
    {
        $qso = Qso::with('contest')
            ->whereHas('contest',function(Builder $q) use( $contestService ) {
                $q->where('contest_id', $contestService->getContestId() );
            })
            ->orderByDesc('time')
            ->first();
        return response()->json([ 'num' => $contestService->numCalc( $qso, $band_id ) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, UserSettings $userSettings, ContestService $contestService)
    {
        Validator::make($request->all(), [
            'rec_call' => ['required'],
            'band_id' => ['required'],
            'mode_id' => ['required'],
            'contest.rec_exchange' => ['required'],
        ])->validate();

        $qso = new Qso( $request->except('id') );
        $qso->user_id = $userSettings->getUserId();
        $qso->type = $userSettings->getMode();
        $qso->my_call = 'R6HZ';
        $qso->my_rep = '59';
        $qso->rec_rep = '59';
        $qso->time = now()->timezone('utc');

        if ( $qso->save() ) {
            $contest = new QsoContest( $request->contest );
            $contest->contest_id = $contestService->getContestId();
            $qso->contest()->save($contest);
        }

        return redirect()->back();

    }

    /**
     * Show the form for creating a new resource.
     *
     */

    public function update(Request $request)
    {

        Validator::make($request->all(), [
            'rec_call' => ['required'],
            'band_id' => ['required'],
            'mode_id' => ['required'],
        ])->validate();

        if ($request->has('id')) {

            $qso = Qso::find($request->input('id'));
            $qso->update($request->all());
            $contest = $qso->contest();
            $contest->update( $request->contest );

            return redirect()->back()
                    ->with('flash.message', 'Updated Successfully.');
        }

    }


}
