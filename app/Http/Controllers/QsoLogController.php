<?php

namespace App\Http\Controllers;


use App\Models\Band;
use App\Models\Mode;
use App\Models\QsoLog;
use App\Services\UserSettings;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Qso;
use Illuminate\Support\Facades\Validator;


class QsoLogController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     */
    public function index( UserSettings $userSettings )
    {

        $log = Qso::with('log')
            ->whereHas('log')
            ->orderByDesc('time')
            ->get();

        $bands = Band::all()->sortBy('order')->values();
        $modes = Mode::all()->sortBy('order')->values();

        $qso = $log->first();

        $new_rec = [
            'band_id' => $qso ? $qso->band_id : 40,
            'mode_id' => $qso ? $qso->mode_id : 'SSB',
            'my_rep' => $qso ? $qso->my_rep : '59',
            'log' => [
                'my_name' => $qso ? $qso->log->my_name : '',
            ],
            'time' => now(),
        ];

        $settings = [
            'rig_apis' => $userSettings->getRigApis(),
            'rig_used' => $userSettings->getRigUsed(),
            'qth' => $userSettings->getQTH(),
        ];

        return Inertia::render('QsoLog', [
            'log' => $log, 'bands' => $bands, 'modes' => $modes, 'new_rec' => $new_rec, 'settings' => $settings,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, UserSettings $userSettings)
    {
        Validator::make($request->all(), [
            'rec_call' => ['required'],
            'band_id' => ['required'],
            'mode_id' => ['required'],
            'my_rep' => ['required'],
            'rec_rep' => ['required'],
            'log.my_name' => ['required'],
            'log.rec_name' => ['required'],
            'log.rec_loc_place' => ['required'],
        ])->validate();

        $qso = new Qso( $request->except('id') );
        $qso->user_id = $userSettings->getUserId();
        $qso->type = $userSettings->getMode();
        $qso->my_call = 'R6HZ';
        $qso->time = now()->timezone('utc');

        if ( $qso->save() ) {
            $log = new QsoLog( $request->log );
            $qso->log()->save($log);
        }

        return redirect()->back();

    }

    /**
     * Show the form for creating a new resource.
     *
     */

    public function update(Request $request)
    {

        Validator::make($request->all(), [
            'rec_call' => ['required'],
            'band_id' => ['required'],
            'mode_id' => ['required'],
            'my_rep' => ['required'],
            'rec_rep' => ['required'],
            'log.my_name' => ['required'],
            'log.rec_name' => ['required'],
            'log.rec_loc_place' => ['required'],
        ])->validate();

        if ($request->has('id')) {

            $qso = Qso::find($request->input('id'));
            $qso->update($request->all());
            $log = $qso->log();
            $log->update( $request->log );

            return redirect()->back()
                    ->with('flash.message', 'Updated Successfully.');
        }

    }


}
