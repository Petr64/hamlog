<?php

namespace App\Http\Controllers\Settings;


use App\Http\Controllers\Controller;
use App\Models\Qso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use App\Models\Band;
use Illuminate\Support\Facades\Validator;


class BandController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Band::orderBy('order')->get()->append('used');
        return Inertia::render('Settings/Bands', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'id' => ['required', 'unique:bands', 'max:3'],
            'title' => ['required'],
        ])->validate();

        Band::create($request->all());

        return redirect()->back()
                    ->with('flash.message', 'Created Successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function update(Request $request)
    {

        Validator::make($request->all(), [
            'id' => ['required', 'max:3'],
            'title' => ['required'],
        ])->validate();

        if ($request->has('id')) {

            Band::find($request->input('id'))->update($request->all());

            return redirect()->back()
                    ->with('flash.message', 'Updated Successfully.');
        } else {
            return redirect()->back()->with('flash.error', 'Update failed.');
        }

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function destroy($id)
    {
        if ( Band::find($id) ) {

            Band::find($id)->delete();
            return redirect()->back()->with('flash.message', 'Deleted Successfully.');;
        }
        return redirect()->back()->with('flash.error', 'Delete failed.');
    }

}
