<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Band;
use App\Models\Contest;
use App\Models\Mode;
use App\Models\Qso;
use App\Services\ContestService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ExportImportController extends Controller
{

    public function exportContest( ContestService $contestService )
    {
        $name = $contestService->getContest()->name . ' - ' . $contestService->getContest()->start;

        return response()->streamDownload(function () use ( $contestService ) {
            $qsos = $contestService->getQsoByContest();
            $bands = $qsos->pluck('band_id')->unique();
            $modes = $qsos->pluck('mode_id')->unique();
            echo collect([
                'bands' => Band::query()->whereIn('id', $bands)->get()->toArray(),
                'modes' => Mode::query()->whereIn('id', $modes)->get()->toArray(),
                'contest' => $contestService->getContest()->toArray(),
                'qso' => $qsos->toArray(),
            ])->toJson();
        }, $name . '.json');

    }


    public function importContest( Request $request, ContestService $contestService )
    {
        $jsonData = json_decode(
            $request->file('import_file')->get()
        );

        $data = $this->toCollect( $jsonData );

        $this->importBands( $data->get('bands') );
        $this->importModes( $data->get('modes') );
        if ( $request->get('new_contest') )
        {
            $contestId = $this->importContestData( $data->get('contest') );
        }
        else
        {
            $contestId = $contestService->getContestId();
        }

        $this->importQsos( $data->get('qso'), $contestId );

        return redirect()->back();

    }

    private function toCollect( array|object $var ): Collection
    {

        return collect( $var )
            ->map(function ($val){
               return (is_array($val) || is_object($val)) ? $this->toCollect( $val ) : $val;
            });

    }

    private function importBands(?Collection $bands): void
    {
        $bands->each(function ($item) {
            Band::updateOrCreate(
                ['id' => $item->get('id')],
                $item->toArray()
            );
        });
    }

    private function importModes(?Collection $modes): void
    {
        $modes->each(function ($item) {
            Mode::updateOrCreate(
                ['id' => $item->get('id')],
                $item->toArray()
            );
        });
    }

    private function importContestData ( Collection $contest ): int
    {
        $contestData = $contest->except('id');
        $model = new Contest();
        $model->fill($contestData->toArray())->push();

        return $model->id;
    }

    private function importQsos ( Collection $qsos, int $contestId ): void
    {

        $qsos->except('id')->each( function ( $item ) use ( $contestId ) {

            $qsoData = $item
                ->except(['id','user_id','contest','log'])
                ->put('user_id', Auth::id())
            ;

            $qso = Qso::updateOrCreate(
               [
                   'my_call' => $qsoData->get('my_call'),
                   'time' => $qsoData->get('time'),
               ],
               $qsoData->toArray()
            );

            if ( $qso->isContest() )
            {
                $contestData = $item->get('contest')->except(['id','qso_id','contest_id'])->put('contest_id', $contestId);
                $qso->contest()->updateOrCreate(
                    ['qso_id' => $qso->id],
                    $contestData->toArray()
                );
            }
            elseif ( $qso->isLog() )
            {
                $logData = $item->get('log')->except(['id','qso_id']);
                $qso->log()->updateOrCreate(
                    ['qso_id' => $qso->id],
                    $logData->toArray()
                );
            }

        });
    }


}
