<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Contest;
use App\Services\UserSettings;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LogSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index( UserSettings $userSettings )
    {

        $settings = $userSettings->getSettings();
        $contests = Contest::all();

        return Inertia::render('Settings/LogSettings', [ 'settings' => $settings, 'contests' => $contests ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param UserSettings $userSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store( Request $request, UserSettings $userSettings)
    {

        $userSettings->setSettings( $request->all() );

        return redirect()->route('qso.index');

    }


}
