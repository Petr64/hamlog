<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\ContestType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ContestTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ContestType::orderBy('id')->get()->append('used');
        return Inertia::render('Settings/ContestTypes', ['data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'id' => ['required', 'unique:bands', 'max:3'],
            'title' => ['required'],
        ])->validate();

        ContestType::create($request->all());

        return redirect()->back()
            ->with('flash.message', 'Created Successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'id' => ['required', 'max:3'],
            'title' => ['required'],
        ])->validate();

        if ($request->has('id')) {

            ContestType::find($request->input('id'))->update($request->all());

            return redirect()->back()
                ->with('flash.message', 'Updated Successfully.');
        } else {
            return redirect()->back()->with('flash.error', 'Update failed.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( ContestType::find($id) ) {

            ContestType::find($id)->delete();
            return redirect()->back()->with('flash.message', 'Deleted Successfully.');;
        }
        return redirect()->back()->with('flash.error', 'Delete failed.');
    }
}
