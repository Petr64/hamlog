<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Contest;
use App\Models\ContestType;
use App\Services\ContestService;
use App\Services\UserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contests = Contest::orderBy('id')->get()->append('used');
        $contest_types = ContestType::all();
        return Inertia::render('Settings/Contests', ['contests' => $contests, 'contest_types' => $contest_types]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required'],
            'contest_type_id' => ['required'],
        ])->validate();

        Contest::create($request->all());

        return redirect()->back()
            ->with('flash.message', 'Created Successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'id' => ['required'],
            'name' => ['required'],
            'contest_type_id' => ['required'],
        ])->validate();

        if ($request->has('id')) {

            Contest::find($request->input('id'))->update($request->all());

            return redirect()->back()
                ->with('flash.message', 'Updated Successfully.');
        } else {
            return redirect()->back()->with('flash.error', 'Update failed.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserSettings $userSettings)
    {
        if ( Contest::find($id) ) {

            if ( $userSettings->getContestId() == $id )
            {
                $userSettings->setContestId( Contest::query()->get()->first()->id );
            }

            Contest::find($id)->delete();
            return redirect()->back()->with('flash.message', 'Deleted Successfully.');;

        }
        return redirect()->back()->with('flash.error', 'Delete failed.');
    }
}
