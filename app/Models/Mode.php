<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mode extends Model
{
    use HasFactory;

    protected static $used_modes;

    protected $fillable = ['id', 'title', 'order'];
    protected $keyType = 'string';

    public function getUsedAttribute()
    {

        if ( empty( static::$used_modes ) )
            static::$used_modes = DB::table('qsos')
                ->selectRaw('mode_id, count(id) as cnt')
                ->groupBy('mode_id')
                ->get()
                ->pluck('mode_id')
                ->toArray();

        if (in_array($this->id, static::$used_modes)) {
            return true;
        }

        return false;
    }


}
