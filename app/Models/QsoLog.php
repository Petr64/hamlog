<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QsoLog extends Model
{
    use HasFactory;

    protected $fillable = [ 'my_name', 'rec_name', 'rec_loc_place', 'data'];

    protected $casts = [
        'data' => 'array',
    ];

    public function qso()
    {
        return $this->belongsTo(Qso::class);
    }

}
