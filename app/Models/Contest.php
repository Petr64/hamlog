<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contest extends Model
{
    use HasFactory;

    protected static $used_contests;

    protected $fillable = ['name', 'locator', 'network', 'start', 'contest_type_id', 'data'];
    protected $casts = [
        'data' => 'array',
    ];

#    public function getDataAttribute ( $data ){
#        return json_encode(['tour_number' => 0, 'tour_duration' => 30]);
#    }

    public function setLocatorAttribute($data)
    {
        $this->attributes['locator'] = is_null($data) ? false : $data;
    }

    public function setNetworkAttribute($data)
    {
        $this->attributes['network'] = is_null($data) ? false : $data;
    }

    public function getUsedAttribute()
    {

        if ( empty( static::$used_contests ) )
            static::$used_contests = DB::table('qso_contests')
                ->selectRaw('contest_id, count(id) as cnt')
                ->groupBy('contest_id')
                ->get()
                ->pluck('contest_id')
                ->toArray();

        if (in_array($this->id, static::$used_contests)) {
            return true;
        }

        return false;
    }

}
