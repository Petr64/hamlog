<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContestType extends Model
{
    use HasFactory;

    protected static $used_types;

    protected $fillable = ['id', 'title', 'tx_num_generate'];
    protected $keyType = 'string';

    public function getUsedAttribute()
    {

        if ( empty( static::$used_types ) )
            static::$used_types = DB::table('contests')
                ->selectRaw('contest_type_id, count(id) as cnt')
                ->groupBy('contest_type_id','id')
                ->get()
                ->pluck('contest_type_id')
                ->toArray();

        if (in_array($this->id, static::$used_types)) {
            return true;
        }

        return false;

    }

}
