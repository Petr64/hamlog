<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QsoContest extends Model
{
    use HasFactory;

    protected $fillable = [ 'contest_id', 'tour', 'my_exchange', 'rec_exchange', 'data' ];

    protected $casts = [
      'data' => 'array',
    ];

    /**
     *
     * Связь с родительской записью
     *
     */
    public function qso()
    {
        return $this->belongsTo(Qso::class);
    }

}
