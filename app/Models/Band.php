<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Band extends Model
{
    use HasFactory;

    protected static $used_bands;

    protected $fillable = ['id', 'title', 'order'];
    protected $keyType = 'string';

    public function getUsedAttribute()
    {

        if ( empty( static::$used_bands ) )
            static::$used_bands = DB::table('qsos')
                ->selectRaw('band_id, count(id) as cnt')
                ->groupBy('band_id','id')
                ->orderBy('id')
                ->get()
                ->pluck('band_id')
                ->toArray();

        if (in_array($this->id, static::$used_bands)) {
            return true;
        }

        return false;
    }

}
