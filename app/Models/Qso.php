<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qso extends Model
{
    use HasFactory;

    /**
     * Отношения, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = ['contest','log'];

    protected $fillable = ['id','type','user_id','band_id','mode_id','my_call','my_rep','rec_call','rec_rep','rec_qth','time'];

    public function isContest(): bool
    {
        return $this->attributes['type'] === 'contest';
    }

    public function isLog(): bool
    {
        return $this->attributes['type'] === 'log';
    }

    /**
     *
     * Связь с дополнительными полями
     *
     */
    public function contest()
    {
        return $this->hasOne(QsoContest::class);
    }

    public function log()
    {
        return $this->hasOne(QsoLog::class);
    }

}
