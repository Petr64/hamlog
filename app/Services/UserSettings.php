<?php

namespace App\Services;


use Illuminate\Support\Facades\Auth;

class UserSettings
{

    protected ?\Illuminate\Contracts\Auth\Authenticatable $user;
    protected array $settings;

    function __construct()
    {

        $this->user = Auth::user();
        $this->settings = $this->user ? $this->user->settings : [];
        $this->settings['rig_apis'] = $this->settings['rig_apis'] ?? [0=>null, 1=>null];
        $this->settings['rig_used'] = $this->settings['rig_used'] ?? false;
        $this->settings['mode'] = $this->settings['mode'] ?? 'contest';
        $this->settings['contest_id'] = $this->settings['contest_id'] ?? 0;
        $this->settings['qth'] = $this->settings['qth'] ?? '';

    }

    /**
     * @return string
     */
    public function getQTH(): string
    {
        return $this->settings['qth'];
    }

    /**
     * @return bool
     */
    public function getRigUsed(): bool
    {
        return $this->settings['rig_used'];
    }

    /**
     * @return int
     */
    public function getContestId(): int
    {
        return $this->settings['contest_id'];
    }

    /**
     * @return void
     */
    public function setContestId( int $contest_id ): void
    {
        $this->settings['contest_id'] = $contest_id;
        $this->setSettings();
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->settings['mode'];
    }

    /**
     * @return array
     */
    public function getRigApis(): array
    {
        return $this->settings['rig_apis'];
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function getUser(): ?\Illuminate\Contracts\Auth\Authenticatable
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user->id;
    }

    /**
     * @return array|mixed
     */
    public function getSettings() : array
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings = []): void
    {
        $this->settings = array_merge($this->settings, $settings);
        $this->user->settings = $this->settings;
        $this->user->update();
    }


}
