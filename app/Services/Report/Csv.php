<?php

namespace App\Services\Report;

use App\Services\ContestService;
use Carbon\Carbon;

class Csv
{

    public static function getContent( ContestService $contestService, $band_id ): string
    {
        $temp  = "[REG1TEST;1]\n";
        $temp .= "TName=\n";
        $temp .= "PCall=R6HZ\n";
        $temp .= "PSect=MULTI-OP\n";
        $temp .= "PBand=ALL\n";
        $temp .= "PWWLo=LN24\n";
        $temp .= "RName=МОУ ДОД БгСЮТ г.Буденновска\n";
        $temp .= "RHBBS=RZ6HZB@budennovsk.ru, ra6fz@bk.ru\n";
        $temp .= "PAdr1=аб/ящик 2\n";
        $temp .= "PAdr2=Буденновск СК 356800\n";

        $log = $contestService->getQsoByContest( $band_id );

        foreach( $log as $qso ){

            $qsoTime = new Carbon( $qso->time );
            $temp2 = substr($qsoTime->toTimeString(), 0, 5);
            $hour = substr($qsoTime->toTimeString(),0, 2);
            $minute = substr($temp2, -2);
            $temp2 = $hour . $minute;

            $temp .= $qsoTime->format('dmy') . ';';
            $temp .= $temp2 . ';';
            $temp .= strtoupper($qso->rec_call) . ';';
            $temp .= '6' . ';';
            $temp .= '59' . ';';
            $temp .= substr($qso->contest->my_exchange, -3, 3) . ';';
            $temp .= '59' . ';';
            $temp .= substr($qso->contest->rec_exchange, -3, 3) . ';';
            $temp .= '' . ';';
            $temp .= strtoupper($qso->rec_qth) . ';';
            $temp .= '' . ';';
            $temp .= '' . ';';
            $temp .= '' . ';';
            $temp .= '' . ';';
            $temp .= '' . ';';

            $temp .= "\n";
        }

        $temp .= "[END;]\n";

        return $temp;

    }

}
