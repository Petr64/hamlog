<?php

namespace App\Services\Report;

use App\Services\ContestService;
use Carbon\Carbon;

class Cabrillo
{

    public static function getContent( ContestService $contestService ): string
    {
        $temp  = "START-OF-LOG: "."\n";
        $temp .= "CONTEST: "."\n";
        $temp .= "CALLSIGN: "."\n";
        $temp .= "CATEGORY-OPERATOR: MULTI-OP\n";
        $temp .= "CATEGORY-BAND: ALL\n";
        $temp .= "CATEGORY-MODE: MIXE\n";
        $temp .= "CATEGORY-OVERLAY: JR\n";
        $temp .= "LOCATION: LN24\n";
        $temp .= "CLUB: МОУ ДОД БгСЮТ г.Буденновска\n";
        $temp .= "EMAIL: RZ6HZB@budennovsk.ru, ra6fz@bk.ru\n";
        $temp .= "NAME: Селищев И А\n";
        $temp .= "ADDRESS: аб/ящик 2\n";
        $temp .= "ADDRESS: Буденновск СК 356800\n";
        $temp .= "OPERATORS: \n";
        $temp .= "OPERATORS: Селищев, Игорь, Андреевич, 1959, 1, RA6FZ,КМС,тренер\n";
        $temp .= "CREATED-BY: HamWebLog\n";

        $log = $contestService->getQsoByContest();
        $fNum = ( $contestService->getContestTypeId() == 'YOC' ) ? 3 : 2;

        foreach( $log as $qso ){

            $qsoTime = new Carbon( $qso->time );
            $temp2 = substr($qsoTime->toTimeString(), 0, 5);
            $hour   = substr($qsoTime->toTimeString(),0, 2);
            $minute = substr($temp2, -2);
            $temp2 = $hour . $minute;

            $temp .= 'QSO: ';
            $temp .= sprintf("%5s", preg_replace("/\d\d\d$/", "", $qso->contest->data['freq']) ) . ' ';
            $temp .= ' PH ';
            $temp .= $qsoTime->format('Y-m-d') . ' ';
            $temp .= $temp2 . ' ';
            $temp .= strtoupper(sprintf("%-13s",$qso->my_call)) . ' ';
            $temp .= substr($qso->contest->my_exchange,0, $fNum) . ' ';
            $temp .= sprintf("%6s",substr($qso->contest->my_exchange, $fNum,3)) . ' ';
            $temp .= strtoupper(sprintf("%-13s",$qso->rec_call)) . ' ';
            $temp .= substr($qso->contest->rec_exchange,0, $fNum) . ' ';
            $temp .= sprintf("%6s",substr($qso->contest->rec_exchange, $fNum,3)) . ' ';


            $temp .= "\n";
        }

        $temp .= "END-OF-LOG:\n";

        return $temp;

    }

}
