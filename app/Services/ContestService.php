<?php

namespace App\Services;

use App\Models\Contest;
use App\Models\Qso;
use App\Models\QsoContest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class ContestService
{

    protected Contest $contest;
    protected Carbon $startTime;
    protected string $tourDuration;

    function __construct( UserSettings $userSettings )
    {
        $contest_id = $userSettings->getContestId();

        if ( ! $this->contest = Contest::find( $contest_id ) )
        {
            return redirect()->route('log_settings.index')->with( 'flash.error', 'Select contest!.' );
        }

        $this->startTime = Carbon::createFromTimeString( $this->getStart(), 'UTC' );
        $t = new Carbon();
        $e = $t->clone();
        $this->tourDuration = $t->diff($e->addMinutes( $this->getTourDurationInMinutes() ))->format("%H:%I");

    }

    /**
     * @return Carbon
     */
    public function getStartTime(): Carbon
    {
        return $this->startTime;
    }

    /**
     * @return string
     */
    public function getTourDurationInISO(): string
    {
        return $this->tourDuration;
    }

    /**
     * @return bool
     */
    public function hasQthLocator(): bool
    {
        return $this->contest->locator;
    }

    /**
     * @return int
     */
    public function getTourDurationInMinutes(): int
    {
        return $this->contest->data['tour_duration'];
    }

    /**
     * @return int
     */
    public function getTourCount(): int
    {
        return $this->contest->data['tour_count'];
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->contest->start;
    }

    /**
     * @return Contest
     */
    public function getContest(): Contest
    {
        return $this->contest;
    }

    /**
     * @return string
     */
    public function getContestTypeId(): string
    {
        return $this->contest->contest_type_id;
    }

    public function needNumRefresh(): bool
    {
        return (boolean) ( $this->contest->contest_type_id == 'IBN' );
    }

    /**
     * @return int
     */
    public function getContestId(): int
    {
        return $this->contest->id;
    }

    /**
     * @param Qso|null $qso
     * @return string
     */
    public function numCalc( ?Qso $qso, $band_id = null ) : string
    {

        switch ( $this->getContestTypeId() ) {
            case 'STD':
                $my_exchange = '59001';
                if ($qso) {
                    $my_num  = (int) substr($qso->contest->my_exchange, 2, 3) + 1;
                    $my_exchange = '59' . sprintf("%03d", $my_num);
                }
                break;

            case 'OPC':
                $my_exchange = '14001';
                if ($qso) {
                    $my_num  = (int) substr($qso->contest->my_exchange, 2, 3) + 1;
                    $my_exchange = '14' . sprintf("%03d", $my_num);
                }
                break;

            case 'YOC':
                $my_exchange = '001000';
                if ($qso) {
                    $rec_str = (int) substr($qso->contest->rec_exchange, 0, 3);
                    $my_num  = (int) substr($qso->contest->my_exchange, 0, 3) + 1;
                    $my_exchange = sprintf("%03d", $my_num) . sprintf("%03d", $rec_str);
                }
                break;

            case 'IBN':
                $my_exchange = '59001';
                if ( ! $band_id ) $band_id = $qso->band_id;

                if ($qso) {
                    $last_qso = QsoContest::where('contest_id', $qso->contest->contest_id)
                        ->whereHas('qso', function(Builder $q) use( $band_id ) {
                            $q->where('band_id', $band_id );
                        })
                        ->orderBy('id','desc')
                        ->first();
                    if ( $last_qso ) {
                        $my_num  = (int) substr($last_qso->my_exchange, 2, 3) + 1;
                        $my_exchange = '59' . sprintf("%03d", $my_num);
                    }
                }
                break;

            default:
                $my_exchange = 'No rule for ' . $this->getContestTypeId();
                break;
        }

        return $my_exchange;
    }


    /**
     * @return array
     */
    public function getQsoByContest( $band_id = null ) : Collection
    {
        $contest_id = $this->getContestId();
//todo Переделать band_id, сделать метод фильтра...
        $qsos = Qso::query()
            ->whereHas('contest', function ($query) use ( $contest_id ){
                $query->where('contest_id', '=', $contest_id);
            })
            ->orderBy('time')
            ->with('contest')
        ;

        if ( $band_id )
        {
            $qsos->where('band_id', '=', $band_id);
        }

        return $qsos->get();

    }

}
