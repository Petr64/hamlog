<?php

namespace App\Services;

use App\Models\Contest;
use App\Models\Qso;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class LogService
{

    function __construct( UserSettings $userSettings )
    {

    }


    /**
     * @return array
     */
    public function getQso( $band_id = null ) : Collection
    {

        $qsos = Qso::query()
            ->orderBy('time')
            ->with('log')
        ;

        if ( $band_id )
        {
            $qsos->where('band_id', '=', $band_id);
        }

        return $qsos->get();

    }

}
