<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->group(function (){

    Route::resource('band', Controllers\Settings\BandController::class);
    Route::resource('mode', Controllers\Settings\ModeController::class);
    Route::resource('contest_type', Controllers\Settings\ContestTypeController::class);

    Route::resource('contest', Controllers\Settings\ContestController::class);
    Route::resource('log_settings', Controllers\Settings\LogSettingsController::class);

    Route::resource('qso', Controllers\QsoController::class);
    Route::resource('reports', Controllers\ReportController::class)->only(['index']);

    Route::get('get_num/{band_id}', [ Controllers\QsoContestController::class, 'get_num'] )
        ->name('get_num');

    Route::get('export', [ Controllers\Settings\ExportImportController::class, 'exportContest'] )
        ->name('export');

    Route::post('import', [ Controllers\Settings\ExportImportController::class, 'importContest' ])
        ->name('import');

    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

});


