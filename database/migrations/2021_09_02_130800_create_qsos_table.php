<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQsosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qsos', function (Blueprint $table) {

            $table->id()->autoIncrement();
            $table->foreignId('user_id')->comment('User ID')->constrained();
            $table->enum('type',['contest','log'])->comment('Type of record');
            $table->string('band_id',3)->comment('Band ID')->foreign('band_id')->references('id')->on('bands');
            $table->string('mode_id',4)->comment('Mode ID')->foreign('mode_id')->references('id')->on('modes');
            $table->string('my_call', 16)->comment('My Callsign')->foreign('my_call')->references('id')->on('callsigns');
            $table->string('my_rep', 4)->comment('Send report');
            $table->string('rec_call', 16)->comment('Remote Callsign')->foreign('rec_call')->references('id')->on('callsigns');
            $table->string('rec_rep', 4)->comment('Recv report');
            $table->string('rec_qth', 8)->comment('Recv QTH')->nullable();
            $table->dateTime('time')->comment('QSO Time');
            $table->timestamps();

            $table->index('user_id');
            $table->index('band_id');
            $table->index('mode_id');
            $table->index('my_call');
            $table->index('rec_call');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qsos');
    }
}
