<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {
            $table->id();
            $table->string('name', '128')->comment('ContestController name');
            $table->boolean('locator')->comment('Use QTH locator')->default(false);
            $table->boolean('network')->comment('Many workplaces with network exchange')->default(false);
            $table->date('date')->comment('Date of contest')->nullable();
            $table->string('contest_type_id',3)->comment('ContestController Type ID');
            $table->jsonb('data')->comment('Other data')->nullable();
            $table->timestamps();

            $table->foreign('contest_type_id')->references('id')->on('contest_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests');
    }
}
