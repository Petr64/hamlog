<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQsoLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qso_logs', function (Blueprint $table) {

            $table->id()->autoIncrement();
            $table->foreignId('qso_id')->comment('QSO ID')->constrained()->onUpdate('cascade')->onDelete('cascade');;
            $table->string('my_name', 32)->comment('Send operator name');
            $table->string('rec_name', 32)->comment('Recv operator name');
            $table->string('rec_loc_place', 32)->comment('Recv simple location');
            $table->jsonb('data')->comment('Other datas')->nullable();
            $table->timestamps();

            $table->index('my_name');
            $table->index('rec_name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qso_logs');
    }
}
