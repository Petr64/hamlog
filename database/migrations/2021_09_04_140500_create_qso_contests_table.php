<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQsoContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qso_contests', function (Blueprint $table) {

            $table->id()->autoIncrement();
            $table->foreignId('qso_id')->comment('QSO ID')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('contest_id')->comment('ContestController ID')->constrained();
            $table->tinyInteger('tour')->comment('Tour Number');
            $table->string('my_exchange', 16)->comment('Send exchange number');
            $table->string('rec_exchange', 16)->comment('Recv exchange number');
            $table->jsonb('data')->comment('Other data')->nullable();
            $table->timestamps();

            $table->index('contest_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qso_contests');
    }
}
