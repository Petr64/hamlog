<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modes')->insert(['order'=>10,'id'=>'SSB','title'=>'SSB']);
        DB::table('modes')->insert(['order'=>20,'id'=>'FM','title'=>'FM']);
        DB::table('modes')->insert(['order'=>30,'id'=>'CW','title'=>'CW']);
        DB::table('modes')->insert(['order'=>40,'id'=>'RTTY','title'=>'RTTY']);
        DB::table('modes')->insert(['order'=>50,'id'=>'PKT','title'=>'Packet']);
        DB::table('modes')->insert(['order'=>60,'id'=>'AMTR','title'=>'AMTOR']);
    }
}
