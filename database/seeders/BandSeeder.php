<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bands')->insert(['order'=>10,'id'=>'160','title'=>'1.8 Mhz']);
        DB::table('bands')->insert(['order'=>20,'id'=>'80','title'=>'3.5 Mhz']);
        DB::table('bands')->insert(['order'=>30,'id'=>'40','title'=>'7.0 Mhz']);
        DB::table('bands')->insert(['order'=>40,'id'=>'20','title'=>'14 Mhz']);
        DB::table('bands')->insert(['order'=>50,'id'=>'15','title'=>'21 Mhz']);
        DB::table('bands')->insert(['order'=>60,'id'=>'10','title'=>'28 Mhz']);
        DB::table('bands')->insert(['order'=>70,'id'=>'2','title'=>'144 Mhz']);
        DB::table('bands')->insert(['order'=>80,'id'=>'07','title'=>'430 Mhz']);
        DB::table('bands')->insert(['order'=>90,'id'=>'125','title'=>'2.4 Ghz']);
    }
}
