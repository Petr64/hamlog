<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contest_types')->insert([
            'id' => 'STD',
            'title' => 'Standart',
            'tx_num_generate' => '',
        ]);

        DB::table('contest_types')->insert([
            'id' => 'OPC',
            'title' => 'Operator age + record num',
            'tx_num_generate' => '',
        ]);

        DB::table('contest_types')->insert([
            'id' => 'YOC',
            'title' => 'Recieved num + my record num',
            'tx_num_generate' => '',
        ]);

        DB::table('contest_types')->insert([
            'id' => 'IBN',
            'title' => 'Individual numbers for band',
            'tx_num_generate' => '',
        ]);

    }
}
