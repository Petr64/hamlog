<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name'=>'Petr',
            'email'=>'petr@budennovsk.ru',
            'password'=>'$2y$10$K9pTIlm7wqjfKrD6fWWTPebX2Hk9lz1f8BoF4jby9buqspZvIwLkG',
            'current_team_id'=>1,
        ]);
    }
}
