<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'id'=>1,
            'user_id'=>1,
            'name'=>'Petr team',
            'personal_team'=>true,
        ]);
    }
}
